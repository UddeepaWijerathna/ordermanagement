package com.company.service.impl;

import com.company.bo.Order;
import com.company.dao.BuyerDao;
import com.company.dao.OrderDao;
import com.company.dao.ProductDao;
import com.company.service.OrderService;

import java.util.Date;

public class OrderServiceImpl implements OrderService {


    ProductDao productDao;
    BuyerDao buyerDao;
    OrderDao orderDao;

    public OrderServiceImpl() {
        productDao = ProductDao.getInstance();
        buyerDao = BuyerDao.getInstance();
        orderDao = OrderDao.getInstance();
    }


    @Override
    public void placeAnOrder(int buyerId, int sellerId, int productId, double orderedQuantity, Date orderedDate) {
        orderDao.addOrder(new Order(buyerId,sellerId,productId,orderedQuantity,orderedDate));
        productDao.decreaseProductQuantity(productId,orderedQuantity);

    }
}
