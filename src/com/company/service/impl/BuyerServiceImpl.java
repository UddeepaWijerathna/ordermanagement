package com.company.service.impl;

import com.company.dao.BuyerDao;
import com.company.dao.OrderDao;
import com.company.dao.ProductDao;
import com.company.exception.InvalidInputException;
import com.company.service.BuyerService;

import java.util.Scanner;

public class BuyerServiceImpl implements BuyerService {


    ProductDao productDao;
    BuyerDao buyerDao;
    OrderDao orderDao;

    public BuyerServiceImpl() {
        productDao = ProductDao.getInstance();
        buyerDao = BuyerDao.getInstance();
        orderDao = OrderDao.getInstance();
    }

    @Override
    public void viewProductList() {
        productDao.getProducts();
    }

    @Override
    public void viewOrdersPlacedByUser() {
        try {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter Your Buyer Id : ");
            int buyerId = scanner.nextInt();
            orderDao.getOrdersForBuyer(buyerId);
        }catch(Exception e){
            throw new InvalidInputException();
        }
    }

    @Override
    public void viewOrdersOfaParticularSeller() {
        try {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter Seller Id : ");
            int sellerId = scanner.nextInt();
            orderDao.getOrdersForSeller(sellerId);
        }catch(Exception e){
            throw new InvalidInputException();
        }
    }
}
