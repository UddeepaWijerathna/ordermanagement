package com.company.service;

public interface BuyerService {
    void viewProductList();
    void viewOrdersPlacedByUser();
    void viewOrdersOfaParticularSeller();
}
