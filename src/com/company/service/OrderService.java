package com.company.service;

import java.util.Date;

public interface OrderService {
    void placeAnOrder(int buyerId, int sellerId, int productId, double orderedQuantity, Date orderedDate);
}
