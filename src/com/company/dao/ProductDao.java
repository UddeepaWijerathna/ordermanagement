package com.company.dao;

import com.company.bo.Order;
import com.company.bo.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.function.Consumer;

public class ProductDao {
    private List<Product> products;

    private static ProductDao productDao;

    private ProductDao() {
        this.products = new ArrayList<Product>();
    }

    public static ProductDao getInstance() {
        productDao = productDao == null ? new ProductDao() : productDao;
        return productDao;
    }

    //Create products
    public void initStoreItems(Product product) {
        products.add(product);
    }

    //Returns the list of all the available products
    public void getProducts(){
        ListIterator<Product> iterator = products.listIterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

    }


    public void removeProduct(Product product) {
        products.remove(product);
    }


    public Product findProduct(int productId) {
        Product product = null;
        for (int index = 0; index <= products.size(); index++) {
            product = products.get(index);
            if (product.getProductId() == productId)
                break;
        }

        return product;
    }

    public boolean isProductValid(Product product){
        if(products.contains(product))
            return true;
        else
            return false;
    }

    public void decreaseProductQuantity(int productId,  double quantity){
        Product product = null;
        for (int index = 0; index <= products.size(); index++) {
            product = products.get(index);
            double availableQuantity= product.getAvailableQuantity()-quantity;
            if (product.getProductId() == productId)
            products.set(index, new Product(productId,product.getSellerId(),product.getProductName(),product.getProductPrice(),quantity));
        }
    }


}
