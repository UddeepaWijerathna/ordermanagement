package com.company.dao;

import com.company.bo.Buyer;
import com.company.bo.Order;

import java.util.ArrayList;
import java.util.List;

public class BuyerDao {
    private List<Buyer> buyers;

    private static BuyerDao buyerDao;

    private BuyerDao() {
        this.buyers = new ArrayList<Buyer>();
    }

    public static BuyerDao getInstance() {
        buyerDao = buyerDao == null ? new BuyerDao() : buyerDao;
        return buyerDao;
    }

    public void addBuyer(Buyer buyer) {
        buyers.add(buyer);
    }


    public void removeBuyer(Buyer buyer) {
        buyers.remove(buyer);
    }

    //Returns the buyer details for the given buyerId
    public Buyer getBuyerDetailsById(int buyerId) {
        Buyer buyer = null;
        for (int index = 0; index <= buyers.size(); index++) {
            buyer = buyers.get(index);
            if (buyer.getBuyerId() == buyerId)
                break;
        }

        return buyer;
    }




}
