package com.company.dao;

import com.company.bo.Seller;

import java.util.ArrayList;
import java.util.List;

public class SellerDao {
    private List<Seller> sellers;

    private static SellerDao sellerDao;

    private SellerDao() {
        this.sellers = new ArrayList<Seller>();
    }

    public static SellerDao getInstance() {
        sellerDao = sellerDao == null ? new SellerDao() : sellerDao;
        return sellerDao;
    }

    public void addSeller(Seller seller) {
        sellers.add(seller);
    }


    public void removeSeller(Seller seller) {
        sellers.remove(seller);
    }
}
