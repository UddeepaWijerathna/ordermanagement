package com.company.dao;

import com.company.bo.Order;
import com.company.bo.SortOrderByBuyer;
import com.company.bo.SortOrderBySeller;

import java.util.*;

public class OrderDao {

    private List<Order> orders;

    private static OrderDao orderDao;

    private OrderDao() {
        this.orders = new ArrayList<Order>();
    }

    public static OrderDao getInstance() {
        orderDao = orderDao == null ? new OrderDao() : orderDao;
        return orderDao;
    }

    public void addOrder(Order order) {
        orders.add(order);
    }


    public void removeOrder(Order order) {
        orders.remove(order);
    }

    //Returns the order for the given orderId
    public Order getOrderDetailsById(int orderId) {
        Order order = null;
        for (int index = 0; index <= orders.size(); index++) {
            order = orders.get(index);
            if (order.getOrderId() == orderId)
                break;
        }

        return order;
    }

    //Returns the list of orders for the given BuyerId
    public void getOrdersForBuyer(int buyerId){
        SortOrderByBuyer sortOrderByBuyer = new SortOrderByBuyer();
        Collections.sort(orders,sortOrderByBuyer);
        for(Order order: orders)
            System.out.println(order);

    }

    //Returns the list of orders for the given SellerId
    public void getOrdersForSeller(int sellerId){
        SortOrderBySeller sortOrderBySeller = new SortOrderBySeller();
        Collections.sort(orders,sortOrderBySeller);
        for(Order order: orders)
            System.out.println(order);
    }

    public void updateOrder(int orderId, int buyerId,int sellerId, int productId, double orderedQuantity, Date orderedDate) {
        for (int index = 0; index <= orders.size(); index++)
            if (orders.get(index).getOrderId() == orderId)
                orders.set(index, new Order(buyerId, sellerId, productId,orderedQuantity,orderedDate));
    }

    public boolean isOrderValid(Order order){
        if(orders.contains(order))
            return true;
        else
            return false;
    }



}
