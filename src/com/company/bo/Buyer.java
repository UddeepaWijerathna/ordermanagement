package com.company.bo;

import java.util.Objects;

public class Buyer {
    final int buyerId;
    final String buyerName;

    public Buyer(int buyerId, String buyerName) {
        this.buyerId = buyerId;
        this.buyerName = buyerName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Buyer)) return false;
        Buyer buyer = (Buyer) o;
        return getBuyerId() == buyer.getBuyerId() &&
                Objects.equals(getBuyerName(), buyer.getBuyerName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBuyerId(), getBuyerName());
    }

    public int getBuyerId() {
        return buyerId;
    }

    public String getBuyerName() {
        return buyerName;
    }
}

