package com.company.bo;

import java.util.Objects;

public class Seller {
    final int sellerId;
    final String sellerName;

    public Seller(int sellerId, String sellerName) {
        this.sellerId = sellerId;
        this.sellerName = sellerName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Seller)) return false;
        Seller seller = (Seller) o;
        return getSellerId() == seller.getSellerId() &&
                Objects.equals(getSellerName(), seller.getSellerName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSellerId(), getSellerName());
    }

    public int getSellerId() {
        return sellerId;
    }

    public String getSellerName() {
        return sellerName;
    }
}
