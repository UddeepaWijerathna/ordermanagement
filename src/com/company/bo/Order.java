package com.company.bo;

import java.util.Date;
import java.util.Objects;

public class Order implements Comparable<Order>{
    private int orderId =0;
    private static int autoIncId = 0;
    private final int buyerId;
    private final int sellerId;
    private final int productId;
    private final double orderedQuantity;
    private final Date orderedDate;

    public Order(int buyerId, int sellerId, int productId, double orderedQuantity, Date orderedDate) {
        this.orderId = autoIncrementId();
        this.buyerId = buyerId;
        this.sellerId = sellerId;
        this.productId = productId;
        this.orderedQuantity = orderedQuantity;
        this.orderedDate = orderedDate;
    }

    @Override
    public int compareTo(Order o) {
        if (this.orderedQuantity > o.orderedQuantity)
            return 1;
        else
            return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;
        Order order = (Order) o;
        return getOrderId() == order.getOrderId() &&
                getBuyerId() == order.getBuyerId() &&
                sellerId == order.sellerId &&
                getProductId() == order.getProductId() &&
                Double.compare(order.getOrderedQuantity(), getOrderedQuantity()) == 0 &&
                Objects.equals(getOrderedDate(), order.getOrderedDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOrderId(), getBuyerId(), sellerId, getProductId(), getOrderedQuantity(), getOrderedDate());
    }

    public int getOrderId() {
        return orderId;
    }

    public int getBuyerId() {
        return buyerId;
    }

    public int getSellerId() {
        return sellerId;
    }

    public int getProductId() {
        return productId;
    }

    public double getOrderedQuantity() {
        return orderedQuantity;
    }

    public Date getOrderedDate() {
        return orderedDate;
    }

    public int autoIncrementId() {
        return ++autoIncId;
    }


}
