package com.company.bo;

import java.util.Objects;

public class Product {
    final int productId;
    final int sellerId;
    final String productName;
    final double productPrice;
    final double availableQuantity;

    public Product(int productId, int sellerId, String productName, double productPrice, double availableQuantity) {
        this.productId = productId;
        this.sellerId = sellerId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.availableQuantity = availableQuantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product product = (Product) o;
        return getProductId() == product.getProductId() &&
                getSellerId() == product.getSellerId() &&
                Double.compare(product.getProductPrice(), getProductPrice()) == 0 &&
                Double.compare(product.getAvailableQuantity(), getAvailableQuantity()) == 0 &&
                Objects.equals(getProductName(), product.getProductName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getProductId(), getSellerId(), getProductName(), getProductPrice(), getAvailableQuantity());
    }

    public int getProductId() {
        return productId;
    }

    public int getSellerId() {
        return sellerId;
    }

    public String getProductName() {
        return productName;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public double getAvailableQuantity() {
        return availableQuantity;
    }
}
