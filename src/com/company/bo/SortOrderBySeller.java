package com.company.bo;

import java.util.Comparator;

public class SortOrderBySeller implements Comparator<Order> {
    @Override
    public int compare(Order o1, Order o2) {
        return o1.getSellerId()-o2.getSellerId();
    }
}
